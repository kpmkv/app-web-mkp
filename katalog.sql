-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2020 at 04:17 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `katalog`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenjang`
--

CREATE TABLE `tbl_jenjang` (
  `id` int(2) NOT NULL,
  `nama` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jenjang`
--

INSERT INTO `tbl_jenjang` (`id`, `nama`) VALUES
(1, 'SD'),
(2, 'SMP'),
(3, 'SMA/K');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `id` int(2) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id`, `nama`) VALUES
(1, 'TEMATIK'),
(2, 'NON-TEMATIK');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kelas`
--

CREATE TABLE `tbl_kelas` (
  `id` int(2) NOT NULL,
  `nama` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kelas`
--

INSERT INTO `tbl_kelas` (`id`, `nama`) VALUES
(9001, 1),
(9002, 2),
(9003, 3),
(9004, 4),
(9005, 5),
(9006, 6),
(9007, 7),
(9008, 8),
(9009, 9),
(9010, 10),
(9011, 11);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kurikulum`
--

CREATE TABLE `tbl_kurikulum` (
  `id` int(2) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kurikulum`
--

INSERT INTO `tbl_kurikulum` (`id`, `nama`) VALUES
(1, 'KTSP'),
(2, 'K-13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_master_buku`
--

CREATE TABLE `tbl_master_buku` (
  `id` int(3) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `isbn` varchar(20) NOT NULL,
  `type` int(2) NOT NULL,
  `kategori` int(2) NOT NULL,
  `kurikulum` int(4) NOT NULL,
  `jenjang` varchar(10) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `semester` varchar(6) NOT NULL,
  `jumlah_halaman` int(3) NOT NULL,
  `pengarang` varchar(60) NOT NULL,
  `editor` varchar(60) NOT NULL,
  `overview` varchar(1000) NOT NULL,
  `tanggal_terbit` date NOT NULL,
  `path_foto` varchar(100) NOT NULL,
  `path_daftarisi` varchar(100) NOT NULL,
  `bahasa` varchar(30) NOT NULL,
  `penerbit` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_master_buku`
--

INSERT INTO `tbl_master_buku` (`id`, `nama`, `isbn`, `type`, `kategori`, `kurikulum`, `jenjang`, `kelas`, `semester`, `jumlah_halaman`, `pengarang`, `editor`, `overview`, `tanggal_terbit`, `path_foto`, `path_daftarisi`, `bahasa`, `penerbit`) VALUES
(1, 'BUKU PENDAMPING BAHASA INDONESIA Untuk Siswa SMP/MTs Kelas VIII Semester 2', '978-602-6355-78-2', 2, 2, 2, '2', '9007', '2', 64, 'Hari Setiawan, S.S.', 'Utsman Amrulloh Al Haniid, S.S.', 'Materi buku ini kami sesuaikan dengan Kurikulum 2013 dan kami kerjakan dengan hati-hati dan selektif. Terdapat 4 pokok pembelajaran, yaitu :', '2019-07-04', '', '', 'Indonesia', 'CV Media Karya Putra'),
(2, 'BUKU PENDAMPING BAHASA INNGRIS Untuk Siswa SMP/MTs Kelas VIII Semester 2', '978-602-6355-54-6', 2, 2, 2, '2', '9008', '1', 64, 'Riris Sumarna, S.Pd,. M.A.', 'Utsman Amrulloh Al Haniid, S.S.', 'Materi buku ini kami sesuaikan dengan Kurikulum 2013 dan kami kerjakan dengan hati-hati dan selektif. Terdapat 7 pokok pembelajaran (chapter), yaitu :\r\n1.      MY UNCLE IS A ZOOKEEPER\r\n2.      WHAT ARE YOU DOING\r\n3.      BIGGEST IS NOT ALWAYS BETTER\r\n4.      WHEN I WAS CHILD\r\n5.      YES, WE MADE IT!\r\n6.      DON\'T FORGET IT, PLEASE!\r\n7.      WE GOT A LOT OF HISTORY \r\nUjian Kompetensi (Competency Test) setiap bab ada di akhir sebelum halaman bab baru. Penilaian Tengah Semester (Mid Semester Test) dan Penilaian Akhir Semester (Final Semester Test) juga tersedia dalam buku ini untuk membantu siswa menilai kemampuan pemahaman materi yang ada di buku.\r\n', '2019-08-07', '', '', 'Inggris', 'CV MEDIA KARYA PUTRA'),
(3, 'BUKU PENDAMPING BAHASA JAWA Untuk Siswa SMP/MTs Kelas VIII Semester 2', '', 2, 2, 2, '2', '9008', '1', 64, 'Nur Tia Sari, S.Pd.', 'Utsman Amrulloh Al Haniid, S.S.', 'Materi buku ini kami sesuaikan dengan Kurikulum 2013 dan kami kerjakan dengan hati-hati dan selektif. Terdapat 4 pokok pembelajaran, yaitu :\r\n1.      CRITA RESI JATHAYU\r\n2.      SERAT WULANGREH PUPUH KINANTHI\r\n3.      LEGENDHA\r\n4.      KOMUNIKASI \r\nUjian Kompetensi setiap bab ada di akhir sebelum halaman bab baru. Penilaian Tengah Semester dan Penilaian Akhir Semester juga tersedia dalam buku ini untuk membantu siswa menilai kemampuan pemahaman materi yang ada di buku.\r\n', '2019-08-07', '', '', 'Jawa', 'CV MEDIA KARYA PUTRA'),
(4, 'BUKU PENDAMPING BIMBINGAN KONSELING Untuk Siswa SMP/MTs Kelas VIII Semester 2', '', 2, 2, 2, '2', '9008', '1', 64, 'Anggit Wiratna Putri, S.Si.', 'Utsman Amrulloh Al Haniid, S.S.', 'Materi buku ini kami sesuaikan dengan Kurikulum 2013 dan kami kerjakan dengan hati-hati dan selektif. Terdapat 4 pokok pembelajaran, yaitu :\r\n1.      BUDAYA DISIPLIN\r\n2.      KEMATANGAN EMOSI REMAJA\r\n3.      KESEHATAN REMAJA\r\n4.      GAYA BELAJAR \r\nUjian Kompetensi setiap bab ada di akhir sebelum halaman bab baru. Penilaian Tengah Semester dan Penilaian Akhir Semester juga tersedia dalam buku ini untuk membantu siswa menilai kemampuan pemahaman materi yang ada di buku.\r\n', '2019-08-07', '', '', 'Indonesia', 'CV MEDIA KARYA PUTRA'),
(5, 'BUKU PENDAMPING ILMU PENGETAHUAN ALAM Untuk Siswa SMP/MTs Kelas VIII Semester 2', '978-602-6355-66-9', 2, 2, 2, '2', '9008', '1', 80, 'Ika Rianti, S.Si.', 'Utsman Amrulloh Al Haniid, S.S.', 'Materi buku ini kami sesuaikan dengan Kurikulum 2013 dan kami kerjakan dengan hati-hati dan selektif. Terdapat 4 pokok pembelajaran, yaitu :\r\n1.      TEKANAN ZAT DAN PENERAPANNYA DALAM KEHIDUPAN SEHARI-HARI\r\n2.      SISTEM PERNAPASAN MANUSIA\r\n3.      SISTEM EKSKRESI MANUSIA\r\n4.      GETARAN, BUNYI, DAN GELOMBANG DALAM KEHIDUPAN SEHARI - HARI\r\n5.       CAHAYA DAN ALAT OPTIK\r\nUjian Kompetensi setiap bab ada di akhir sebelum halaman bab baru. Penilaian Tengah Semester dan Penilaian Akhir Semester juga tersedia dalam buku ini untuk membantu siswa menilai kemampuan pemahaman materi yang ada di buku.\r\n', '2019-08-07', '', '', 'Indonesia', 'CV MEDIA KARYA PUTRA'),
(6, 'BUKU PENDAMPING ILMU PENGETAHUAN SOSIAL Untuk Siswa SMP/MTs Kelas VIII Semester 2', '978-602-6355-81-2', 2, 2, 2, '2', '9008', '1', 80, 'Alfian Tamara Putra, S.Pd.', 'Utsman Amrulloh Al Haniid, S.S.', 'Materi buku ini kami sesuaikan dengan Kurikulum 2013 dan kami kerjakan dengan hati-hati dan selektif. Terdapat 2 pokok pembelajaran, yaitu :\r\n1.      KEUNGGULAN DAN KETERBATASAN ANTARRUANG PENGARUHNYA TERHADAP KEGIATAN EKONOMI SOSIAL\r\n2.      PERUBAHAN MASYARAKAT INDONESIA PADA MASA PENJAJAHAN DAN TUMBUHNYA SEMANGAT KEBANGSAAN \r\nUjian Kompetensi setiap bab ada di akhir sebelum halaman bab baru. Penilaian Tengah Semester dan Penilaian Akhir Semester juga tersedia dalam buku ini untuk membantu siswa menilai kemampuan pemahaman materi yang ada di buku.\r\n', '2019-08-07', '', '', 'Indonesia', 'CV MEDIA KARYA PUTRA'),
(7, 'BUKU PENDAMPING MATEMATIKA Untuk Siswa SMP/MTs Kelas VIII Semester 2', '978-602-635574-2', 2, 2, 2, '2', '9008', '1', 61, 'Imaroh Aprilia, S.Si.', 'Utsman Amrulloh Al Haniid, S.S.', 'Materi buku ini kami sesuaikan dengan Kurikulum 2013 dan kami kerjakan dengan hati-hati dan selektif. Terdapat 5 pokok pembelajaran, yaitu :\r\n1.      TEOREMA PHYTAGORAS\r\n2.      LINGKARAN\r\n3.      BANGUN RUANG SISI DATAR\r\n4.      STATISTIKA\r\n5.      PELUANG \r\nUjian Kompetensi setiap bab ada di akhir sebelum halaman bab baru. Penilaian Tengah Semester dan Penilaian Akhir Semester juga tersedia dalam buku ini untuk membantu siswa menilai kemampuan pemahaman materi yang ada di buku.\r\n', '2019-08-07', '', '', 'Indonesia', 'CV MEDIA KARYA PUTRA'),
(8, 'BUKU PENDAMPING PENJASORKES Untuk Siswa SMP/MTs Kelas VIII Semester 2', '978-602-6355-62-1', 2, 2, 2, '2', '9008', '1', 64, 'Eko Sapto Nugroho, S. Pd.', 'Utsman Amrulloh Al Haniid, S.S.', 'Materi buku ini kami sesuaikan dengan Kurikulum 2013 dan kami kerjakan dengan hati-hati dan selektif. Terdapat 4 pokok pembelajaran, yaitu :\r\n1.      AKTIVITAS SENAM\r\n2.      AKTIVITAS RENANG\r\n3.       PENCEGAHAN PERGAULAN BEBAS\r\n4.      KESELAMATAN DI JALAN RAYA\r\nUjian Kompetensi setiap bab ada di akhir sebelum halaman bab baru. Penilaian Tengah Semester dan Penilaian Akhir Semester juga tersedia dalam buku ini untuk membantu siswa menilai kemampuan pemahaman materi yang ada di buku.\r\n', '2019-08-07', '', '', 'Indonesia', 'CV MEDIA KARYA PUTRA'),
(9, 'BUKU PENDAMPING PPKn Untuk Siswa SMP/MTs Kelas VIII Semester 2', '', 2, 2, 2, '2', '9008', '1', 64, 'Hari Setiawan, S.S.', 'Utsman Amrulloh Al Haniid, S.S.', 'Materi buku ini kami sesuaikan dengan Kurikulum 2013 dan kami kerjakan dengan hati-hati dan selektif. Terdapat 3 pokok pembelajaran, yaitu :\r\n1.      SEMANGAT KEBANGKITAN NASIONAL TAHUN 1908\r\n2.      SUMPAH PEMUDA DALAM BINGKAI BHINEKA TUNGGAL IKA\r\n3.      MEMPERKUAT KOMITMEN KEBANGSAAN\r\nUjian Kompetensi setiap bab ada di akhir sebelum halaman bab baru. Penilaian Tengah Semester dan Penilaian Akhir Semester juga tersedia dalam buku ini untuk membantu siswa menilai kemampuan pemahaman materi yang ada di buku.\r\n', '2019-08-07', '', '', 'Indonesia', 'CV MEDIA KARYA PUTRA'),
(10, 'BUKU PENDAMPING PRAKARYA Untuk Siswa SMP/MTs Kelas VIII Semester 2', '978-602-6355-58-4', 2, 2, 2, '2', '9008', '1', 64, 'Hari Setiawan, S.S.', 'Utsman Amrulloh Al Haniid, S.S.', 'Materi buku ini kami sesuaikan dengan Kurikulum 2013 dan kami kerjakan dengan hati-hati dan selektif. Terdapat 5 pokok pembelajaran, yaitu :\r\n1.      KERAJINAN BAHAN LIMBAH KERTAS\r\n2.      ALAT PENJERNIH AIR\r\n3.      BUDI DAYA SATWA HARAPAN\r\n4.      PENGOLAHAN BAHAN PANGAN SETENGAH JADI MENJADI MAKANAN KHAS DAERAH SETEMPAT\r\n5.      PENGOLAHAN HASIL SAMPING SEREALIA, KACANG – KACANGAN DAN UMBI MENJADI PRODUK PANGAN\r\nUjian Kompetensi setiap bab ada di akhir sebelum halaman bab baru. Penilaian Tengah Semester dan Penilaian Akhir Semester juga tersedia dalam buku ini untuk membantu siswa menilai kemampuan pemahaman materi yang ada di buku.\r\n', '2019-08-07', '', '', 'Indonesia', 'CV MEDIA KARYA PUTRA'),
(11, 'bahasa jawa', '', 0, 0, 0, '', '', '', 0, '', '', '', '0000-00-00', '', '', '', ''),
(12, 'BUKU TEMATIK KELAS III SD/MI TEMA 1 PERTUMBUHAN DAN PERKEMBANGAN MAKHLUK HIDUP', '978-602-427-179-4', 1, 1, 2, '1', '9003', '2', 202, 'Sonya Sinyanyuri dan Lubna Assagaf', 'Trie Hartiti Retnowati, Bambang Prihadi, Widia Pekerti, Rita', 'Buku teks yang berbasis aktivitas ini disusun sebagai salah satu penunjang penerapan Kurikulum 2013 yang disempurnakan yang sangat mengedepankan pada pencapaian kompetensi siswa sesuai standar kelulusan yang ditetapkan. Buku ini masih membutuhkan buku-buku penunjang guna memperkaya wawasan dan keterampilan siswa. Guru maupun siswa dapat memanfaatkan buku-buku KTSP yang sudah dimiliki sekolah sebagai buku penunjang. Guru maupun siswa juga dapat memanfaatkan bahan – bahan belajar lainnya yg relevan, termasuk ensiklopedia, berbagai buku yang membahas topik terkait pembelajaran, majalah, surat kabar, dan sebagainya', '2018-05-05', '', '', 'Indonesia', 'Pusat Kurikulum dan Perbukuan, Balitbang, Kemendik'),
(13, 'BUKU TEMATIK KELAS III SD/MI TEMA 2 MENYAYANGI TUMBUHAN DAN HEWAN', '978-602-427-189-3', 1, 1, 2, '1', '9003', '2', 219, 'Yanti Kurnianingsih, Sonya Sinyanyuri., dan Lubna Assagaf', 'Achmad Husein, Amat Komari, Anung Priambodo, Asep Supriyana,', 'Buku teks yang berbasis aktivitas ini disusun sebagai salah satu penunjang penerapan Kurikulum 2013 yang disempurnakan yang sangat mengedepankan pada pencapaian kompetensi siswa sesuai standar kelulusan yang ditetapkan. Buku ini masih membutuhkan buku-buku penunjang guna memperkaya wawasan dan keterampilan siswa. Guru maupun siswa dapat memanfaatkan buku-buku KTSP yang sudah dimiliki sekolah sebagai buku penunjang. Guru maupun siswa juga dapat memanfaatkan bahan – bahan belajar lainnya yg relevan, termasuk ensiklopedia, berbagai buku yang membahas topik terkait pembelajaran, majalah, surat kabar, dan sebagainya', '2018-05-05', '', '', 'Indonesia', 'Pusat Kurikulum dan Perbukuan, Balitbang, Kemendik'),
(14, 'BUKU TEMATIK KELAS III SD/MI TEMA 3 BENDA DI SEKITARKU', '978-602-427-193-0', 1, 1, 2, '1', '9003', '2', 242, 'Sari Kusuma Dewi dan Lubna Assagaf', 'Achmad Husein, Widia Pekerti, Rita Milyartini, Esti Swatika ', 'Buku teks yang berbasis aktivitas ini disusun sebagai salah satu penunjang penerapan Kurikulum 2013 yang disempurnakan yang sangat mengedepankan pada pencapaian kompetensi siswa sesuai standar kelulusan yang ditetapkan. Buku ini masih membutuhkan buku-buku penunjang guna memperkaya wawasan dan keterampilan siswa. Guru maupun siswa dapat memanfaatkan buku-buku KTSP yang sudah dimiliki sekolah sebagai buku penunjang. Guru maupun siswa juga dapat memanfaatkan bahan – bahan belajar lainnya yg relevan, termasuk ensiklopedia, berbagai buku yang membahas topik terkait pembelajaran, majalah, surat kabar, dan sebagainya', '2018-05-05', '', '', 'Indonesia', 'Pusat Kurikulum dan Perbukuan, Balitbang, Kemendik'),
(15, 'BUKU TEMATIK KELAS III SD/MI TEMA 4 KEWAJIBAN DAN HAKKU', '978-602-427-185-5', 1, 1, 2, '1', '9003', '2', 200, 'Iba Muhibba dan Lubna Assagaf', 'Suharji, Iim Siti Masyitoh, Setyo Purwanto, Trie Hartiti Ret', 'Buku teks yang berbasis aktivitas ini disusun sebagai salah satu penunjang penerapan Kurikulum 2013 yang disempurnakan yang sangat mengedepankan pada pencapaian kompetensi siswa sesuai standar kelulusan yang ditetapkan. Buku ini masih membutuhkan buku-buku penunjang guna memperkaya wawasan dan keterampilan siswa. Guru maupun siswa dapat memanfaatkan buku-buku KTSP yang sudah dimiliki sekolah sebagai buku penunjang. Guru maupun siswa juga dapat memanfaatkan bahan – bahan belajar lainnya yg relevan, termasuk ensiklopedia, berbagai buku yang membahas topik terkait pembelajaran, majalah, surat kabar, dan sebagainya', '2018-05-05', '', '', 'Indonesia', 'Pusat Kurikulum dan Perbukuan, Balitbang, Kemendik'),
(16, 'BUKU TEMATIK KELAS VI SD/MI TEMA 1 SELAMATKAN MAKHLUK HIDUP', '978-602-427-212-8', 1, 1, 2, '1', '9006', '2', 178, 'Angi St Anggari, Afriki, Dara Retno Wulan, Nuniek Puspitawat', 'Bambang Prihadi, Daru Wahyuni, Eddy Budiono, Encep Supriatna', 'Buku teks yang berbasis aktivitas ini disusun sebagai salah satu penunjang penerapan Kurikulum 2013 yang disempurnakan yang sangat mengedepankan pada pencapaian kompetensi siswa sesuai standar kelulusan yang ditetapkan. Buku ini masih membutuhkan buku-buku penunjang guna memperkaya wawasan dan keterampilan siswa. Guru maupun siswa dapat memanfaatkan buku-buku KTSP yang sudah dimiliki sekolah sebagai buku penunjang. Guru maupun siswa juga dapat memanfaatkan bahan – bahan belajar lainnya yg relevan, termasuk ensiklopedia, berbagai buku yang membahas topik terkait pembelajaran, majalah, surat kabar, dan sebagainya', '2018-05-05', '', '', 'Indonesia', 'Pusat Kurikulum dan Perbukuan, Balitbang, Kemendik');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profil_perusahaan`
--

CREATE TABLE `tbl_profil_perusahaan` (
  `id` int(1) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `telp` varchar(12) NOT NULL,
  `hp` varchar(12) NOT NULL,
  `wa` varchar(12) NOT NULL,
  `ig` varchar(30) NOT NULL,
  `tw` varchar(30) NOT NULL,
  `overall` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reset_password`
--

CREATE TABLE `tbl_reset_password` (
  `id` bigint(20) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activation_id` varchar(32) NOT NULL,
  `agent` varchar(512) NOT NULL,
  `client_ip` varchar(32) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` bigint(20) NOT NULL DEFAULT 1,
  `createdDtm` datetime NOT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) NOT NULL COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
(1, 'System Administrator'),
(2, 'Users');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_semester`
--

CREATE TABLE `tbl_semester` (
  `id` int(2) NOT NULL,
  `nama` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_semester`
--

INSERT INTO `tbl_semester` (`id`, `nama`) VALUES
(1, 'GENAP'),
(2, 'GANJIL');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_type`
--

CREATE TABLE `tbl_type` (
  `id` int(2) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_type`
--

INSERT INTO `tbl_type` (`id`, `nama`) VALUES
(1, 'PAKET'),
(2, 'LKS\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `email`, `password`, `name`, `mobile`, `roleId`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
(1, 'wahyusetiaone27@gmail.com', '$2y$10$0lBJ64CHvYTHHmqaTSNGgePeTPlMTWJqUCOYPZjEDwrKPRUceDhwK', 'Wahyu Setiawan', '000008', 1, 0, 1, '2020-03-28 23:41:17', 1, '2020-03-28 23:47:16'),
(2, 'abahsudahbesar@gmail.com', '$2y$10$YxiGHwDY.3V76kSrQQYbmeTB7A8fl5ub4pYGBLuJRqwc.NU2PuA/O', 'Wahyu Setiawann', '7804922443', 2, 0, 1, '2020-03-28 23:45:19', 1, '2020-03-29 03:44:23'),
(3, 'abahs@gmail.com', '$2y$10$wvUUwF9MWbt0rGVTRiOaoeJgYxcCMHuNSjChG./PNYrQ3/Shk0s3O', 'Abah Setiawan', '7804922443', 2, 1, 1, '2020-03-29 03:16:13', 1, '2020-03-29 03:16:23'),
(4, 'wehaye@gmail.com', '$2y$10$h8qpg9gmN9I9wi8i46Kx5OKGjOfCBG1PnoVYL1dAgeFCHDM4A1xQq', 'Wahyu Setiawan', '7804922443', 2, 0, 1, '2020-03-29 03:40:32', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_master_buku`
--
ALTER TABLE `tbl_master_buku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_profil_perusahaan`
--
ALTER TABLE `tbl_profil_perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`roleId`);

--
-- Indexes for table `tbl_type`
--
ALTER TABLE `tbl_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
