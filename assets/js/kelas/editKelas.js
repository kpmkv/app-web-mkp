/**
 * File : editUser.js 
 * 
 * This file contain the validation of edit user form
 * 
 * @author Kishor Mali
 */
$(document).ready(function(){
	
	var editKelasForm = $("#editKelas");
	
	var validator = editKelasForm.validate({
		
		rules:{
			id :{ required : true },
			nama : { required : true,  remote : { url : baseURL + "checkNamaExists", type :"post", data : { nama : function(){ return $("#nama").val(); } } } }
		},
		messages:{
			id :{ required : "This field is required" },
			nama : { required : "This field is required", remote : "Nama already taken" },
		}
	});
});