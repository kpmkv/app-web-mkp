/**
 * @author Kishor Mali
 */


jQuery(document).ready(function(){
	
	jQuery(document).on("click", ".deleteUser", function(){
		var userId = $(this).data("userid"),
			hitURL = baseURL + "deleteUser",
			currentRow = $(this);
		
		var confirmation = confirm("Are you sure to delete this user ?");
		
		if(confirmation)
		{
			jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			data : { userId : userId } 
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("User successfully deleted"); }
				else if(data.status = false) { alert("User deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});

	jQuery(document).on("click", ".deleteKelas", function(){
		var id = $(this).data("id"),
			hitURL = baseURL + "deleteKelas",
			currentRow = $(this);

		var confirmation = confirm("Are you sure to delete this Kelas ?");

		if(confirmation)
		{
			jQuery.ajax({
				type : "POST",
				dataType : "json",
				url : hitURL,
				data : { id : id }
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Kelas successfully deleted"); }
				else if(data.status = false) { alert("Kelas deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	jQuery(document).on("click", ".deleteKategori", function(){
		var id = $(this).data("id"),
			hitURL = baseURL + "deleteKategori",
			currentRow = $(this);

		var confirmation = confirm("Are you sure to delete this kategori ?");

		if(confirmation)
		{
			jQuery.ajax({
				type : "POST",
				dataType : "json",
				url : hitURL,
				data : { id : id }
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Kategori successfully deleted"); }
				else if(data.status = false) { alert("Kategori deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	
	
	jQuery(document).on("click", ".deleteJenjang", function(){
		var id = $(this).data("id"),
			hitURL = baseURL + "deleteJenjang",
			currentRow = $(this);

		var confirmation = confirm("Are you sure to delete this jenjang ?");

		if(confirmation)
		{
			jQuery.ajax({
				type : "POST",
				dataType : "json",
				url : hitURL,
				data : { id : id }
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("jenjang successfully deleted"); }
				else if(data.status = false) { alert("Jenjang deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});
	
	jQuery(document).on("click", ".deleteKurikulum", function(){
		var id = $(this).data("id"),
			hitURL = baseURL + "deleteKurikulum",
			currentRow = $(this);

		var confirmation = confirm("Are you sure to delete this kurikulum ?");

		if(confirmation)
		{
			jQuery.ajax({
				type : "POST",
				dataType : "json",
				url : hitURL,
				data : { id : id }
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Kurikulum successfully deleted"); }
				else if(data.status = false) { alert("Kurikulum deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});

	jQuery(document).on("click", ".deleteSemester", function(){
		var id = $(this).data("id"),
			hitURL = baseURL + "deleteSemester",
			currentRow = $(this);

		var confirmation = confirm("Are you sure to delete this semester ?");

		if(confirmation)
		{
			jQuery.ajax({
				type : "POST",
				dataType : "json",
				url : hitURL,
				data : { id : id }
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Semester successfully deleted"); }
				else if(data.status = false) { alert("Semester deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});

	jQuery(document).on("click", ".deleteBuku", function(){
		var id = $(this).data("id"),
			hitURL = baseURL + "deleteBuku",
			currentRow = $(this);

		var confirmation = confirm("Are you sure to delete this buku ?");

		if(confirmation)
		{
			jQuery.ajax({
				type : "POST",
				dataType : "json",
				url : hitURL,
				data : { id : id }
			}).done(function(data){
				console.log(data);
				currentRow.parents('tr').remove();
				if(data.status = true) { alert("Buku successfully deleted"); }
				else if(data.status = false) { alert("Buku deletion failed"); }
				else { alert("Access denied..!"); }
			});
		}
	});

});
