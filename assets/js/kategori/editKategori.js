/**
 * File : addUser.js
 *
 * This file contain the validation of add user form
 *
 * Using validation plugin : jquery.validate.js
 *
 * @author Kishor Mali
 */

$(document).ready(function(){

    var addKategoriForm = $("#addKategori");

    var validator = addKategoriForm.validate({

        rules:{
            nama : { required : true, remote : { url : baseURL + "checkNamaExists", type :"post"} }
        },
        messages:{
            nama : { required : "This field is required", email : "Please enter valid email address", remote : "Nama already taken" }
        }
    });
});
