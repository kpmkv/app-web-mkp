<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require (APPPATH.'/libraries/RestController.php');

use chriskacerguis\RestServer\RestController;

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Api extends RestController
{
    /**
     * This is default constructor of the class
     */
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('semester_model');
        $this->load->database();
    }

    //url : http://templatecilte.so/api/semester
    //url : http://templatecilte.so/api/semester/id using $id = $this->get( 'id' );

    function semester_get(){
        $result = $this->semester_model->get();

        if (!empty($result)){
            $data = array("status"=>true,"message"=>"NO ERROR", "results"=>$result);
        }else{
            $data = array("status"=>false,"message"=>"ERROR", "results"=>array());
        }

        $this->response($data, 200);
    }

    function lks_get(){
        $query = "select
       m.id as id,
       m.nama as nama,
       m.isbn as isbn,
       tt.nama as type,
       tk.nama as kategori,
       k.nama as kurikulum,
       tj.nama as jenjang,
       t.nama as kelas,
       ts.nama as semester,
       m.jumlah_halaman as jumlah_halaman,
       m.pengarang as pengarang,
       m.editor as editor,
       m.overview as overview,
       m.tanggal_terbit as tanggal_terbit,
       m.path_foto as path_foto,
       m.path_daftarisi as path_daftarisi,
       m.bahasa as bahasa,
       m.penerbit as penerbit
       from
            tbl_master_buku m inner join tbl_jenjang tj on m.jenjang = tj.id
            inner join tbl_kategori tk on m.kategori = tk.id
            inner join tbl_kelas t on m.kelas = t.id
            inner join tbl_kurikulum k on m.kurikulum = k.id
            inner join tbl_semester ts on m.semester = ts.id
            inner join tbl_type tt on m.type = tt.id";
        $querySearch = "select
       m.id as id,
       m.nama as nama,
       m.isbn as isbn,
       tt.nama as type,
       tk.nama as kategori,
       k.nama as kurikulum,
       tj.nama as jenjang,
       t.nama as kelas,
       ts.nama as semester,
       m.jumlah_halaman as jumlah_halaman,
       m.pengarang as pengarang,
       m.editor as editor,
       m.overview as overview,
       m.tanggal_terbit as tanggal_terbit,
       m.path_foto as path_foto,
       m.path_daftarisi as path_daftarisi,
       m.bahasa as bahasa,
       m.penerbit as penerbit
       from
            tbl_master_buku m inner join tbl_jenjang tj on m.jenjang = tj.id
            inner join tbl_kategori tk on m.kategori = tk.id
            inner join tbl_kelas t on m.kelas = t.id
            inner join tbl_kurikulum k on m.kurikulum = k.id
            inner join tbl_semester ts on m.semester = ts.id
            inner join tbl_type tt on m.type = tt.id where m.nama like ?";

        $search = $this->get( 'search' );

        if ($search != null || $search != ""){

            $results = $this->db->query($querySearch, array("%".$search."%"));
            $values = $results->result();
            $data = array("status"=>true,"message"=>"NO ERROR", "results"=>$values);

        }else{

            $results = $this->db->query($query);
            $values = $results->result();
            $data = array("status"=>true,"message"=>"NO ERROR", "results"=>$values);
        }

        $this->response($data, 200);
    }

    function paket_get(){
        $query = "select
       m.id as id,
       m.nama as nama,
       m.isbn as isbn,
       tt.nama as type,
       tk.nama as kategori,
       k.nama as kurikulum,
       tj.nama as jenjang,
       t.nama as kelas,
       ts.nama as semester,
       m.jumlah_halaman as jumlah_halaman,
       m.pengarang as pengarang,
       m.editor as editor,
       m.overview as overview,
       m.tanggal_terbit as tanggal_terbit,
       m.path_foto as path_foto,
       m.path_daftarisi as path_daftarisi,
       m.bahasa as bahasa,
       m.penerbit as penerbit
       from
            tbl_master_buku m inner join tbl_jenjang tj on m.jenjang = tj.id
            inner join tbl_kategori tk on m.kategori = tk.id
            inner join tbl_kelas t on m.kelas = t.id
            inner join tbl_kurikulum k on m.kurikulum = k.id
            inner join tbl_semester ts on m.semester = ts.id
            inner join tbl_type tt on m.type = tt.id";
        $querySearch = "select
       m.id as id,
       m.nama as nama,
       m.isbn as isbn,
       tt.nama as type,
       tk.nama as kategori,
       k.nama as kurikulum,
       tj.nama as jenjang,
       t.nama as kelas,
       ts.nama as semester,
       m.jumlah_halaman as jumlah_halaman,
       m.pengarang as pengarang,
       m.editor as editor,
       m.overview as overview,
       m.tanggal_terbit as tanggal_terbit,
       m.path_foto as path_foto,
       m.path_daftarisi as path_daftarisi,
       m.bahasa as bahasa,
       m.penerbit as penerbit
       from
            tbl_master_buku m inner join tbl_jenjang tj on m.jenjang = tj.id
            inner join tbl_kategori tk on m.kategori = tk.id
            inner join tbl_kelas t on m.kelas = t.id
            inner join tbl_kurikulum k on m.kurikulum = k.id
            inner join tbl_semester ts on m.semester = ts.id
            inner join tbl_type tt on m.type = tt.id where m.nama like ?";

        $search = $this->get( 'search' );

        if ($search != null || $search != ""){

            $results = $this->db->query($querySearch, array("%".$search."%"));
            $values = $results->result();
            $data = array("status"=>true,"message"=>"NO ERROR", "results"=>$values);

        }else{

            $results = $this->db->query($query);
            $values = $results->result();
            $data = array("status"=>true,"message"=>"NO ERROR", "results"=>$values);
        }

        $this->response($data, 200);
    }

    function buku_get(){
        $query = "select
       m.id as id,
       m.nama as nama,
       m.isbn as isbn,
       tt.nama as type,
       tk.nama as kategori,
       k.nama as kurikulum,
       tj.nama as jenjang,
       t.nama as kelas,
       ts.nama as semester,
       m.jumlah_halaman as jumlah_halaman,
       m.pengarang as pengarang,
       m.editor as editor,
       m.overview as overview,
       m.tanggal_terbit as tanggal_terbit,
       m.path_foto as path_foto,
       m.path_daftarisi as path_daftarisi,
       m.bahasa as bahasa,
       m.penerbit as penerbit
       from
            tbl_master_buku m inner join tbl_jenjang tj on m.jenjang = tj.id
            inner join tbl_kategori tk on m.kategori = tk.id
            inner join tbl_kelas t on m.kelas = t.id
            inner join tbl_kurikulum k on m.kurikulum = k.id
            inner join tbl_semester ts on m.semester = ts.id
            inner join tbl_type tt on m.type = tt.id";
        $querySearch = "select
       m.id as id,
       m.nama as nama,
       m.isbn as isbn,
       tt.nama as type,
       tk.nama as kategori,
       k.nama as kurikulum,
       tj.nama as jenjang,
       t.nama as kelas,
       ts.nama as semester,
       m.jumlah_halaman as jumlah_halaman,
       m.pengarang as pengarang,
       m.editor as editor,
       m.overview as overview,
       m.tanggal_terbit as tanggal_terbit,
       m.path_foto as path_foto,
       m.path_daftarisi as path_daftarisi,
       m.bahasa as bahasa,
       m.penerbit as penerbit
       from
            tbl_master_buku m inner join tbl_jenjang tj on m.jenjang = tj.id
            inner join tbl_kategori tk on m.kategori = tk.id
            inner join tbl_kelas t on m.kelas = t.id
            inner join tbl_kurikulum k on m.kurikulum = k.id
            inner join tbl_semester ts on m.semester = ts.id
            inner join tbl_type tt on m.type = tt.id order by id limit ?";

        $search = $this->get( 'limited' );

        if ($search != null || $search != ""){

            $results = $this->db->query($querySearch, array("%".$search."%"));
            $values = $results->result();
            $data = array("status"=>true,"message"=>"NO ERROR", "results"=>$values);

        }else{

            $results = $this->db->query($query);
            $values = $results->result();
            $data = array("status"=>true,"message"=>"NO ERROR", "results"=>$values);
        }

        $this->response($data, 200);
    }
}