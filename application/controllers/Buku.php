<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Buku extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('buku_model');
        $this->load->model('user_model');
        $this->load->helper(array('form', 'url'));
        $this->isLoggedIn();
    }

//     function getBuku(){
//         $result = $this->Buku_model->getBuku();
//         if(!is_empty($data)){
//             $data = array("status"=>true,"message"=>"NO ERROR", "results"=>array($result));
//         }else{
//             $data = array("status"=>false,"message"=>"ERROR", "results"=>array();
//         }
//         echo json_encode($data);
//     }

    /** This function is used insert of data Buku */
    function create()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');
//
            $this->form_validation->set_rules('id', 'ID', 'required|min_length[2]|xss_clean');
            $this->form_validation->set_rules('nama', 'Nama Buku', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('isbn', 'NO ISBN', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('type', 'TYPE', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('kategori', 'KATEGORI', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('kurikulum', 'KURIKULUM', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('jenjang', 'JENJANG', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('kelas', 'KELAS', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('semester', 'SEMESTER', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('jumlah_halaman', 'JUM_HAL', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('pengarang', 'PENGARANG', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('editor', 'EDITOR', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('overview', 'OVERVIEW', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('tanggal_terbit', 'TERBIT', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('bahasa', 'BAHASA', 'trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('penerbit', 'PENERBIT', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_create();
            } else {

                $config = array(
                    'upload_path' => "./uploads/",
                    'allowed_types' => "gif|jpg|png|jpeg|pdf",
                    'overwrite' => TRUE,
                    'encrypt_name' => TRUE,
                    'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                    'max_height' => "768",
                    'max_width' => "1024"
                );

                $uri = "uploads/";

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("path_foto")) {
                    $data = array('upload_data' => $this->upload->data());
                    if ($this->upload->do_upload("path_daftarisi")) {
                        $data2 = array('upload_data' => $this->upload->data());

                        $buku_id = $this->input->post('id');
                        $buku_nama = $this->input->post('nama');
                        $isbn = $this->input->post('isbn');
                        $type = $this->input->post('type');
                        $kategori = $this->input->post('kategori');
                        $kurikulum = $this->input->post('kurikulum');
                        $jenjang = $this->input->post('jenjang');
                        $kelas = $this->input->post('kelas');
                        $semester = $this->input->post('semester');
                        $jumlah_halaman = $this->input->post('jumlah_halaman');
                        $pengarang = $this->input->post('pengarang');
                        $editor = $this->input->post('editor');
                        $overview = $this->input->post('overview');
                        $tanggal_terbit = $this->input->post('tanggal_terbit');
                        $path_foto = $uri.$data['upload_data']['file_name'];
                        $path_daftarisi = $uri.$data2['upload_data']['file_name'];
                        $bahasa = $this->input->post('bahasa');
                        $penerbit = $this->input->post('penerbit');

                        $dataBuku = array(
                            'id' => $buku_id,
                            'nama' => $buku_nama,
                            'isbn' => $isbn,
                            'type' => $type,
                            'kategori' => $kategori,
                            'kurikulum' => $kurikulum,
                            'jenjang' => $jenjang,
                            'kelas' => $kelas,
                            'semester' => $semester,
                            'jumlah_halaman' => $jumlah_halaman,
                            'pengarang' => $pengarang,
                            'editor' => $editor,
                            'overview' => $overview,
                            'tanggal_terbit' => $tanggal_terbit,
                            'path_foto' => $path_foto,
                            'path_daftarisi' => $path_daftarisi,
                            'bahasa' => $bahasa,
                            'penerbit' => $penerbit,
                        );

//                        $dataBuku = $this->input->post();

                        $result = $this->buku_model->create($dataBuku);

                        if ($result) {
                            $this->session->set_flashdata('success', 'New Buku created successfully');
                        } else {
                            $this->session->set_flashdata('error', 'Buku creation failed');
                        }

                    }else{
                        $data2 = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('error', $data2);
                    }
                }else{
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error', $data);
                }

                redirect('formTambahBuku');
            }
        }
    }

    /**
     * This function is used to load the Buku list
     */
    function read()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->buku_model->BukuCount($searchText);

            $returns = $this->paginationCompress("daftarBuku/", $count, 5);

            $data['BukuRecords'] = $this->buku_model->read($searchText, $returns["page"], $returns["segment"]);

            $this->global['pageTitle'] = 'CodeInsect : Lists of Buku';

            $this->loadViews("buku/daftar_buku", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to update the Buku information
     */
    function update()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $buku_id = $this->input->post('id');
            $this->form_validation->set_rules('nama', 'Nama Buku', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_update($buku_id);
            } else {
                $buku_nama = $this->input->post('nama');

                $databuku = array(
                    'id' => $buku_id,
                    'nama' => $buku_nama
                );

                $result = $this->buku_model->update($databuku, $buku_id);

                if ($result == true) {
                    $this->session->set_flashdata('success', 'Buku updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Buku updation failed');
                }

                redirect('formEditBuku/' . $buku_id);
            }
        }
    }

    /**
     * This function is used to delete the Buku using id
     * @return boolean $result : TRUE / FALSE
     */
    function delete()
    {
        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $buku_id = $this->input->post('id');

            $result = $this->buku_model->delete($buku_id);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }



    //FUNCTION HELPER

    /** This function is used show form of create data Buku */
    function form_create()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $data['lastedId'] = $this->buku_model->lasted_id();

            $this->global['pageTitle'] = 'CodeInsect : Add New Buku';

            $this->loadViews("buku/tambah_buku", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used load Buku edit information
     * @param number $id : Optional : This is Buku id
     */
    function form_update($id = NULL)
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            if ($id == null) {
                redirect('daftarBuku');
            }

            $data['roles'] = $this->user_model->getUserRoles();
            $data['dataBuku'] = $this->buku_model->show($id);

            $this->global['pageTitle'] = 'CodeInsect : Edit Buku';

            $this->loadViews("buku/update_buku", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to nama already exist or not
     */
    function check_nama_exists()
    {
        $nama = $this->input->post("nama");

        $result = $this->buku_model->check_nama_exists($nama);

        if (empty($result)) {
            echo("true");
        } else {
            echo("false");
        }
    }
}

?>