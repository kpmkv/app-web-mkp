<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Kategori extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kategori_model');
        $this->load->model('user_model');
        $this->isLoggedIn();
    }

    /** This function is used insert of data Kategori */
    function create()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('id', 'ID', 'required|min_length[2]|xss_clean');
            $this->form_validation->set_rules('nama', 'Nama Kategori', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_create();
            } else {

                $Kategori_id = $this->input->post('id');
                $Kategori_nama = $this->input->post('nama');

                $dataKategori = array(
                    'id' => $Kategori_id,
                    'nama' => $Kategori_nama
                );

                $result = $this->Kategori_model->create($dataKategori);

                if ($result) {
                    $this->session->set_flashdata('success', 'New Kategori created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Kategori creation failed');
                }
                redirect('formTambahKategori');
            }
        }
    }

    /**
     * This function is used to load the Kategori list
     */
    function read()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->kategori_model->kategoriCount($searchText);

            $returns = $this->paginationCompress("daftarKategori/", $count, 5);

            $data['KategoriRecords'] = $this->kategori_model->read($searchText, $returns["page"], $returns["segment"]);

            $this->global['pageTitle'] = 'CodeInsect : Lists of Kategori';

            $this->loadViews("Kategori/daftar_Kategori", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to update the Kategori information
     */
    function update()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $Kategori_id = $this->input->post('id');
            $this->form_validation->set_rules('nama', 'Nama Kategori', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_update($Kategori_id);
            } else {
                $Kategori_nama = $this->input->post('nama');

                $dataKategori = array(
                    'id'=>$Kategori_id,
                    'nama' => $Kategori_nama
                );

                $result = $this->kategori_model->update($dataKategori, $Kategori_id);

                if ($result == true) {
                    $this->session->set_flashdata('success', 'Kategori updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Kategori updation failed');
                }

                redirect('formEditKategori/'.$Kategori_id);
            }
        }
    }

    /**
     * This function is used to delete the Kategori using id
     * @return boolean $result : TRUE / FALSE
     */
    function delete()
    {
        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $Kategori_id = $this->input->post('id');

            $result = $this->kategori_model->delete($Kategori_id);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }



    //FUNCTION HELPER

    /** This function is used show form of create data Kategori */
    function form_create()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $data['lastedId'] = $this->kategori_model->lasted_id();

            $this->global['pageTitle'] = 'CodeInsect : Add New Kategori';

            $this->loadViews("Kategori/tambah_Kategori", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used load Kategori edit information
     * @param number $id : Optional : This is Kategori id
     */
    function form_update($id = NULL)
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            if ($id == null) {
                redirect('daftarKategori');
            }

            $data['roles'] = $this->user_model->getUserRoles();
            $data['dataKategori'] = $this->kategori_model->show($id);

            $this->global['pageTitle'] = 'CodeInsect : Edit Kategori';

            $this->loadViews("Kategori/update_Kategori", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to nama already exist or not
     */
    function check_nama_exists()
    {
        $nama = $this->input->post("nama");

        $result = $this->kategori_model->check_nama_exists($nama);

        if (empty($result)) {
            echo("true");
        } else {
            echo("false");
        }
    }
}

?>