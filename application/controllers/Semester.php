<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Semester extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('semester_model');
        $this->load->model('user_model');
        $this->isLoggedIn();
    }

    /** This function is used insert of data Semester */
    function create()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('id', 'ID', 'required|min_length[2]|xss_clean');
            $this->form_validation->set_rules('nama', 'Nama Semester', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_create();
            } else {

                $Semester_id = $this->input->post('id');
                $Semester_nama = $this->input->post('nama');

                $dataSemester = array(
                    'id' => $Semester_id,
                    'nama' => $Semester_nama
                );

                $result = $this->semester_model->create($dataSemester);

                if ($result) {
                    $this->session->set_flashdata('success', 'New Semester created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Semester creation failed');
                }
                redirect('formTambahSemester');
            }
        }
    }

    /**
     * This function is used to load the Semester list
     */
    function read()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->semester_model->SemesterCount($searchText);

            $returns = $this->paginationCompress("daftarSemester/", $count, 5);

            $data['SemesterRecords'] = $this->semester_model->read($searchText, $returns["page"], $returns["segment"]);

            $this->global['pageTitle'] = 'CodeInsect : Lists of Semester';

            $this->loadViews("Semester/daftar_Semester", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to update the Semester information
     */
    function update()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $Semester_id = $this->input->post('id');
            $this->form_validation->set_rules('nama', 'Nama Semester', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_update($Semester_id);
            } else {
                $Semester_nama = $this->input->post('nama');

                $dataSemester = array(
                    'id'=>$Semester_id,
                    'nama' => $Semester_nama
                );

                $result = $this->semester_model->update($dataSemester, $Semester_id);

                if ($result == true) {
                    $this->session->set_flashdata('success', 'Semester updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Semester updation failed');
                }

                redirect('formEditSemester/'.$Semester_id);
            }
        }
    }

    /**
     * This function is used to delete the Semester using id
     * @return boolean $result : TRUE / FALSE
     */
    function delete()
    {
        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $Semester_id = $this->input->post('id');

            $result = $this->semester_model->delete($Semester_id);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }



    //FUNCTION HELPER

    /** This function is used show form of create data Semester */
    function form_create()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $data['lastedId'] = $this->semester_model->lasted_id();

            $this->global['pageTitle'] = 'CodeInsect : Add New Semester';

            $this->loadViews("Semester/tambah_Semester", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used load Semester edit information
     * @param number $id : Optional : This is Semester id
     */
    function form_update($id = NULL)
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            if ($id == null) {
                redirect('daftarSemester');
            }

            $data['roles'] = $this->user_model->getUserRoles();
            $data['dataSemester'] = $this->semester_model->show($id);

            $this->global['pageTitle'] = 'CodeInsect : Edit Semester';

            $this->loadViews("Semester/update_Semester", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to nama already exist or not
     */
    function check_nama_exists()
    {
        $nama = $this->input->post("nama");

        $result = $this->semester_model->check_nama_exists($nama);

        if (empty($result)) {
            echo("true");
        } else {
            echo("false");
        }
    }
}

?>