<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Kelas extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kelas_model');
        $this->load->model('user_model');
        $this->isLoggedIn();
    }

//     function getKelas(){
//         $result = $this->kelas_model->getKelas();
//         if(!is_empty($data)){
//             $data = array("status"=>true,"message"=>"NO ERROR", "results"=>array($result));
//         }else{
//             $data = array("status"=>false,"message"=>"ERROR", "results"=>array();
//         }
//         echo json_encode($data);
//     }

    /** This function is used insert of data kelas */
    function create()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('id', 'ID', 'required|min_length[2]|xss_clean');
            $this->form_validation->set_rules('nama', 'Nama Kelas', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_create();
            } else {

                $kelas_id = $this->input->post('id');
                $kelas_nama = $this->input->post('nama');

                $dataKelas = array(
                    'id' => $kelas_id,
                    'nama' => $kelas_nama
                );

                $result = $this->kelas_model->create($dataKelas);

                if ($result) {
                    $this->session->set_flashdata('success', 'New Kelas created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Kelas creation failed');
                }
                redirect('formTambahKelas');
            }
        }
    }

    /**
     * This function is used to load the kelas list
     */
    function read()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->kelas_model->kelasCount($searchText);

            $returns = $this->paginationCompress("daftarKelas/", $count, 5);

            $data['kelasRecords'] = $this->kelas_model->read($searchText, $returns["page"], $returns["segment"]);

            $this->global['pageTitle'] = 'CodeInsect : Lists of Kelas';

            $this->loadViews("kelas/daftar_kelas", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to update the kelas information
     */
    function update()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $kelas_id = $this->input->post('id');
            $this->form_validation->set_rules('nama', 'Nama Kelas', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_update($kelas_id);
            } else {
                $kelas_nama = $this->input->post('nama');

                $dataKelas = array(
                    'id'=>$kelas_id,
                    'nama' => $kelas_nama
                );

                $result = $this->kelas_model->update($dataKelas, $kelas_id);

                if ($result == true) {
                    $this->session->set_flashdata('success', 'Kelas updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Kelas updation failed');
                }

                redirect('formEditKelas/'.$kelas_id);
            }
        }
    }

    /**
     * This function is used to delete the kelas using id
     * @return boolean $result : TRUE / FALSE
     */
    function delete()
    {
        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $kelas_id = $this->input->post('id');

            $result = $this->kelas_model->delete($kelas_id);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }



    //FUNCTION HELPER

    /** This function is used show form of create data kelas */
    function form_create()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $data['lastedId'] = $this->kelas_model->lasted_id();

            $this->global['pageTitle'] = 'CodeInsect : Add New Kelas';

            $this->loadViews("kelas/tambah_kelas", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used load kelas edit information
     * @param number $id : Optional : This is kelas id
     */
    function form_update($id = NULL)
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            if ($id == null) {
                redirect('daftarKelas');
            }

            $data['roles'] = $this->user_model->getUserRoles();
            $data['dataKelas'] = $this->kelas_model->show($id);

            $this->global['pageTitle'] = 'CodeInsect : Edit Kelas';

            $this->loadViews("kelas/update_kelas", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to nama already exist or not
     */
    function check_nama_exists()
    {
        $nama = $this->input->post("nama");

        $result = $this->kelas_model->check_nama_exists($nama);

        if (empty($result)) {
            echo("true");
        } else {
            echo("false");
        }
    }
}

?>