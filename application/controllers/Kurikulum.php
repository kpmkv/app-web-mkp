<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Kurikulum extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('kurikulum_model');
        $this->load->model('user_model');
        $this->isLoggedIn();
    }

    /** This function is used insert of data Kurikulum */
    function create()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('id', 'ID', 'required|min_length[2]|xss_clean');
            $this->form_validation->set_rules('nama', 'Nama Kurikulum', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_create();
            } else {

                $Kurikulum_id = $this->input->post('id');
                $Kurikulum_nama = $this->input->post('nama');

                $dataKurikulum = array(
                    'id' => $Kurikulum_id,
                    'nama' => $Kurikulum_nama
                );

                $result = $this->kurikulum_model->create($dataKurikulum);

                if ($result) {
                    $this->session->set_flashdata('success', 'New Kurikulum created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Kurikulum creation failed');
                }
                redirect('formTambahKurikulum');
            }
        }
    }

    /**
     * This function is used to load the Kurikulum list
     */
    function read()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->kurikulum_model->KurikulumCount($searchText);

            $returns = $this->paginationCompress("daftarKurikulum/", $count, 5);

            $data['KurikulumRecords'] = $this->kurikulum_model->read($searchText, $returns["page"], $returns["segment"]);

            $this->global['pageTitle'] = 'CodeInsect : Lists of Kurikulum';

            $this->loadViews("Kurikulum/daftar_Kurikulum", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to update the Kurikulum information
     */
    function update()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $Kurikulum_id = $this->input->post('id');
            $this->form_validation->set_rules('nama', 'Nama Kurikulum', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_update($Kurikulum_id);
            } else {
                $Kurikulum_nama = $this->input->post('nama');

                $dataKurikulum = array(
                    'id'=>$Kurikulum_id,
                    'nama' => $Kurikulum_nama
                );

                $result = $this->kurikulum_model->update($dataKurikulum, $Kurikulum_id);

                if ($result == true) {
                    $this->session->set_flashdata('success', 'Kurikulum updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Kurikulum updation failed');
                }

                redirect('formEditKurikulum/'.$Kurikulum_id);
            }
        }
    }

    /**
     * This function is used to delete the Kurikulum using id
     * @return boolean $result : TRUE / FALSE
     */
    function delete()
    {
        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $Kurikulum_id = $this->input->post('id');

            $result = $this->kurikulum_model->delete($Kurikulum_id);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }



    //FUNCTION HELPER

    /** This function is used show form of create data Kurikulum */
    function form_create()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $data['lastedId'] = $this->kurikulum_model->lasted_id();

            $this->global['pageTitle'] = 'CodeInsect : Add New Kurikulum';

            $this->loadViews("Kurikulum/tambah_Kurikulum", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used load Kurikulum edit information
     * @param number $id : Optional : This is Kurikulum id
     */
    function form_update($id = NULL)
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            if ($id == null) {
                redirect('daftarKurikulum');
            }

            $data['roles'] = $this->user_model->getUserRoles();
            $data['dataKurikulum'] = $this->kurikulum_model->show($id);

            $this->global['pageTitle'] = 'CodeInsect : Edit Kurikulum';

            $this->loadViews("Kurikulum/update_Kurikulum", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to nama already exist or not
     */
    function check_nama_exists()
    {
        $nama = $this->input->post("nama");

        $result = $this->kurikulum_model->check_nama_exists($nama);

        if (empty($result)) {
            echo("true");
        } else {
            echo("false");
        }
    }
}

?>