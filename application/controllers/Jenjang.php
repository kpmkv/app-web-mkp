<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Jenjang extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('jenjang_model');
        $this->load->model('user_model');
        $this->isLoggedIn();
    }

    /** This function is used insert of data Jenjang */
    function create()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('id', 'ID', 'required|min_length[2]|xss_clean');
            $this->form_validation->set_rules('nama', 'Nama Jenjang', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_create();
            } else {

                $Jenjang_id = $this->input->post('id');
                $Jenjang_nama = $this->input->post('nama');

                $dataJenjang = array(
                    'id' => $Jenjang_id,
                    'nama' => $Jenjang_nama
                );

                $result = $this->jenjang_model->create($dataJenjang);

                if ($result) {
                    $this->session->set_flashdata('success', 'New Jenjang created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Jenjang creation failed');
                }
                redirect('formTambahJenjang');
            }
        }
    }

    /**
     * This function is used to load the Jenjang list
     */
    function read()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->jenjang_model->JenjangCount($searchText);

            $returns = $this->paginationCompress("daftarJenjang/", $count, 5);

            $data['JenjangRecords'] = $this->jenjang_model->read($searchText, $returns["page"], $returns["segment"]);

            $this->global['pageTitle'] = 'CodeInsect : Lists of Jenjang';

            $this->loadViews("Jenjang/daftar_Jenjang", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to update the Jenjang information
     */
    function update()
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $Jenjang_id = $this->input->post('id');
            $this->form_validation->set_rules('nama', 'Nama Jenjang', 'trim|required|max_length[128]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $this->form_update($Jenjang_id);
            } else {
                $Jenjang_nama = $this->input->post('nama');

                $dataJenjang = array(
                    'id'=>$Jenjang_id,
                    'nama' => $Jenjang_nama
                );

                $result = $this->jenjang_model->update($dataJenjang, $Jenjang_id);

                if ($result == true) {
                    $this->session->set_flashdata('success', 'Jenjang updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Jenjang updation failed');
                }

                redirect('formEditJenjang/'.$Jenjang_id);
            }
        }
    }

    /**
     * This function is used to delete the Jenjang using id
     * @return boolean $result : TRUE / FALSE
     */
    function delete()
    {
        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $Jenjang_id = $this->input->post('id');

            $result = $this->jenjang_model->delete($Jenjang_id);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }



    //FUNCTION HELPER

    /** This function is used show form of create data Jenjang */
    function form_create()
    {

        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $data['lastedId'] = $this->jenjang_model->lasted_id();

            $this->global['pageTitle'] = 'CodeInsect : Add New Jenjang';

            $this->loadViews("Jenjang/tambah_Jenjang", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used load Jenjang edit information
     * @param number $id : Optional : This is Jenjang id
     */
    function form_update($id = NULL)
    {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            if ($id == null) {
                redirect('daftarJenjang');
            }

            $data['roles'] = $this->user_model->getUserRoles();
            $data['dataJenjang'] = $this->jenjang_model->show($id);

            $this->global['pageTitle'] = 'CodeInsect : Edit Jenjang';

            $this->loadViews("Jenjang/update_Jenjang", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to nama already exist or not
     */
    function check_nama_exists()
    {
        $nama = $this->input->post("nama");

        $result = $this->jenjang_model->check_nama_exists($nama);

        if (empty($result)) {
            echo("true");
        } else {
            echo("false");
        }
    }
}

?>