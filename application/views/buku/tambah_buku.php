<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> User Management
            <small>Add / Edit User</small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->


                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Buku Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

<!--                    <form role="form" id="addBuku" action="--><?php //echo base_url() ?><!--tambahBuku" method="post"-->
<!--                          role="form">-->
                    <?php echo form_open_multipart(base_url().'tambahBuku');?>

                    <div class="box-body">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="id">ID</label>
                                        <?php if (!empty($lastedId)) {
                                            ?>
                                            <input type="text" class="form-control required" id="id" name="id"
                                                   minlength="1"
                                                   maxlength="128" value="<?php echo $lastedId; ?>" readonly><?php
                                        } ?>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nama">Nama Buku</label>
                                        <input type="text" class="form-control required" id="nama" name="nama"
                                               maxlength="128">
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="isbn">Nomor ISBN</label>
                                        <input type="text" class="form-control required" id="isbn" name="isbn"
                                               maxlength="128">
                                    </div>

                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="type">Type</label>
                                        <select id="type" name="type" class="form-control required">
                                            <option value="1">PAKET</option>
                                            <option value="2">LKS</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kategori">Kategori</label>
                                        <select id="kategori" name="kategori" class="form-control required">
                                            <option value="1">TEMATIK</option>
                                            <option value="2">NON-TEMATIK</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kurikulum">Kurikulum</label>
                                        <select id="kurikulum" name="kurikulum" class="form-control required">
                                            <option value="1">KTSP</option>
                                            <option value="2">K-13</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jenjang">Jenjang</label>
                                        <select id="jenjang" name="jenjang" class="form-control required">
                                            <option value="1">SD</option>
                                            <option value="2">SMP</option>
                                            <option value="3">SMA/K</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kelas">Kelas</label>
                                        <select id="kelas" name="kelas" class="form-control required">
                                            <option value="9001">1</option>
                                            <option value="9002">2</option>
                                            <option value="9003">3</option>
                                            <option value="9004">4</option>
                                            <option value="9005">5</option>
                                            <option value="9006">6</option>
                                            <option value="9007">7</option>
                                            <option value="9008">8</option>
                                            <option value="9009">9</option>
                                            <option value="9010">10</option>
                                            <option value="9011">11</option>
                                            <option value="9012">12</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="semester">Nomor ISBN</label>
                                        <select id="semester" name="semester" class="form-control required">
                                            <option value="1">GENAP</option>
                                            <option value="2">GANJIL</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jumlah_halaman">Jumlah Halaman</label>
                                        <input type="text" class="form-control required" id="jumlah_halaman"
                                               name="jumlah_halaman"
                                               maxlength="128">
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pengarang">Pengarang</label>
                                        <input type="text" class="form-control required" id="pengarang" name="pengarang"
                                               maxlength="128">
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="editor">Editor</label>
                                        <input type="text" class="form-control required" id="editor" name="editor"
                                               maxlength="128">
                                    </div>

                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="overview">Overview</label>
                                        <input type="text" class="form-control required" id="overview" name="overview"
                                               maxlength="128">
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="tanggal_terbit">Tanggal Terbit</label>
                                        <input type="date" class="form-control required" id="tanggal_terbit"
                                               name="tanggal_terbit">
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="path_foto">Foto Cover</label>
<!--                                        <input type="file" class="form-control required" id="path_foto" name="path_foto">-->
                                        <?php echo "<input type='file' name='path_foto' size='20' />"; ?>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="path_daftarisi">Foto Lain</label>
                                        <?php echo "<input type='file' name='path_daftarisi' size='20' />"; ?>
<!--                                        <input type="text" class="form-control required" id="path_daftarisi"-->
<!--                                               name="path_daftarisi"-->
<!--                                               maxlength="128">-->
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="bahasa">Bahasa</label>
                                        <input type="text" class="form-control required" id="bahasa" name="bahasa"
                                               maxlength="128">
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="penerbit">Penerbit</label>
                                        <input type="text" value="PT Media Karya Putra" readonly
                                               class="form-control required" id="penerbit" name="penerbit"
                                               maxlength="128">
                                    </div>

                                </div>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit"/>
                            <input type="reset" class="btn btn-default" value="Reset"/>
                        </div>
<!--                    </form>-->
                    <?php echo "</form>"?>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if ($error) {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if ($success) {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<script src="<?php echo base_url(); ?>assets/js/       buku/addBuku.js" type="text/javascript"></script>