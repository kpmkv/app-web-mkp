<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Buku Management
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>formTambahBuku"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Buku List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>daftarBuku" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>Id</th>
                      <th>Nama</th>
                      <th>ISBN</th>
                      <th>Type</th>
                      <th>Kategori</th>
                      <th>Kurikulum</th>
                      <th>Jenjang</th>
                      <th>Kelas</th>
                      <th>Semester</th>
                      <th>Jumlah Halaman</th>
                      <th>Pengarang</th>
                      <th>Editor</th>
                      <th>Overview</th>
                      <th>Tanggal Terbit</th>
                      <th>Path Foto</th>
                      <th>Path Daftar Isi</th>
                      <th>Bahasa</th>
                      <th>Penerbit</th>
                      <th class="text-center">Actions</th>
                    </tr>
                    <?php
                    if(!empty($BukuRecords))
                    {
                        foreach($BukuRecords as $record)
                        {
                    ?>
                    <tr>
                      <td><?php echo $record->id ?></td>
                      <td><?php echo $record->nama ?></td>
                      <td><?php echo $record->isbn ?></td>
                      <td><?php echo $record->type ?></td>
                      <td><?php echo $record->kategori ?></td>
                      <td><?php echo $record->kurikulum ?></td>
                      <td><?php echo $record->jenjang ?></td>
                      <td><?php echo $record->kelas ?></td>
                      <td><?php echo $record->semester ?></td>
                      <td><?php echo $record->jumlah_halaman ?></td>
                      <td><?php echo $record->pengarang ?></td>
                      <td><?php echo $record->editor ?></td>
                      <td><?php echo $record->overview ?></td>
                      <td><?php echo $record->tanggal_terbit ?></td>
                      <td><?php echo $record->path_foto ?></td>
                      <td><?php echo $record->path_daftarisi ?></td>
                      <td><?php echo $record->bahasa ?></td>
                      <td><?php echo $record->penerbit ?></td>
                      <td class="text-center">
                          <a class="btn btn-sm btn-info" href="<?php echo base_url().'formEditBuku/'.$record->id; ?>"><i class="fa fa-pencil"></i></a>
                          <a class="btn btn-sm btn-danger deleteBuku" href="#" data-id="<?php echo $record->id; ?>"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "daftarBuku/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
