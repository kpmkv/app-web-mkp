<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_model extends CI_Model
{

    /**
     * This function return all of the record tbl_kategori
     * @param array $dataKategori : This is value of record
     * @return boolean TRUE : This is last inserted id
     */

    function create($dataKategori)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_kategori', $dataKategori);

        $this->db->trans_complete();

        return TRUE;
    }



    /**
     * This function is used to get the kategori listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function read($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('tbl_kategori as tk');
        if(!empty($searchText)) {
            $likeCriteria = "(tk.id  LIKE '%".$searchText."%'
                            OR  tk.nama  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     * This function is used to update the kategori information
     * @param array $dataKategori : This is kategori updated information
     * @param number $id : This is kategori id
     * @return boolean TRUE
     */
    function update($dataKategori, $id)
    {
        $this->db->where('id', $id);

        return $this->db->update('tbl_kategori', $dataKategori);
    }

    /**
     * This function is used to delete the kategori information
     * @param number $id : This is kategori id
     * @return boolean TRUE
     */
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_kategori');

        return TRUE;
    }

    /**
     * This function is used to show the kategori information
     * @param number $id : This is kategori id
     * @return array $result : This is result of query
     */
    function show($id)
    {
        $result = $this->db->get_where('tbl_kategori', array('id' => $id))->result();
        return $result;
    }

    /**
     * This function is used to get the kategori listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */

    function kategoriCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_kategori as tk');
        if(!empty($searchText)) {
            $likeCriteria = "(tk.id  LIKE '%".$searchText."%'
                            OR  tk.nama  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();

        return count($query->result());
    }

    /**
     * This function is used to get lasted id from the kategori information
     * @return  number $id : This is lasted kategori id
     */
    function lasted_id(){
        $this->db->select('id');
        $this->db->from('tbl_kategori');
        $query = $this->db->get()->last_row();
        $id = $query->id + 1;
        return $id;
    }

    /**
     * This function is used to check nama is already exist or not
     * @param {string} $nama : This is nama
     * @return {mixed} $result : This is searched result
     */
    function check_nama_exists_kategori($nama)
    {
        $this->db->select("nama");
        $this->db->from("tbl_kategori");
        $this->db->where("nama", $nama);
        $query = $this->db->get();

        return $query->result();
    }
}

?>
