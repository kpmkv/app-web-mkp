<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jenjang_model extends CI_Model
{

    /**
     * This function return all of the record tbl_jenjang
     * @param array $dataJenjang : This is value of record
     * @return boolean TRUE : This is last inserted id
     */

    function create($dataJenjang)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_jenjang', $dataJenjang);

        $this->db->trans_complete();

        return TRUE;
    }



    /**
     * This function is used to get the jenjang listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function read($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('tbl_jenjang as tj');
        if(!empty($searchText)) {
            $likeCriteria = "(tj.id  LIKE '%".$searchText."%'
                            OR  tj.nama  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     * This function is used to update the jenjang information
     * @param array $dataKelas : This is jenjang updated information
     * @param number $id : This is jenjang id
     * @return boolean TRUE
     */
    function update($dataJenjang, $id)
    {
        $this->db->where('id', $id);

        return $this->db->update('tbl_jenjang', $dataJenjang);
    }

    /**
     * This function is used to delete the jenjang information
     * @param number $id : This is jenjang id
     * @return boolean TRUE
     */
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_jenjang');

        return TRUE;
    }

    /**
     * This function is used to show the jenjang information
     * @param number $id : This is jenjang id
     * @return array $result : This is result of query
     */
    function show($id)
    {
        $result = $this->db->get_where('tbl_jenjang', array('id' => $id))->result();
        return $result;
    }

    /**
     * This function is used to get the jenjang listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */

    function jenjangCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_jenjang as tj');
        if(!empty($searchText)) {
            $likeCriteria = "(tj.id  LIKE '%".$searchText."%'
                            OR  tj.nama  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();

        return count($query->result());
    }

    /**
     * This function is used to get lasted id from the jenjang information
     * @return  number $id : This is lasted jenjang id
     */
    function lasted_id(){
        $this->db->select('id');
        $this->db->from('tbl_jenjang');
        $query = $this->db->get()->last_row();
        $id = $query->id + 1;
        return $id;
    }

    /**
     * This function is used to check nama is already exist or not
     * @param {string} $nama : This is nama
     * @return {mixed} $result : This is searched result
     */
    function check_nama_exists_jenjang($nama)
    {
        $this->db->select("nama");
        $this->db->from("tbl_jenjang");
        $this->db->where("nama", $nama);
        $query = $this->db->get();

        return $query->result();
    }
}

?>
