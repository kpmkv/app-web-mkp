<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kurikulum_model extends CI_Model
{

    /**
     * This function return all of the record tbl_kurikulum
     * @param array $dataKurikulum : This is value of record
     * @return boolean TRUE : This is last inserted id
     */

    function create($dataKurikulum)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_kurikulum', $dataKurikulum);

        $this->db->trans_complete();

        return TRUE;
    }



    /**
     * This function is used to get the kurikulum listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function read($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('tbl_kurikulum as tk');
        if(!empty($searchText)) {
            $likeCriteria = "(tk.id  LIKE '%".$searchText."%'
                            OR  tk.nama  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     * This function is used to update the kurikulum information
     * @param array $dataKategori : This is kurikulum updated information
     * @param number $id : This is kurikulum id
     * @return boolean TRUE
     */
    function update($dataKurikulum, $id)
    {
        $this->db->where('id', $id);

        return $this->db->update('tbl_kurikulum', $dataKurikulum);
    }

    /**
     * This function is used to delete the kurikulum information
     * @param number $id : This is kurikulum id
     * @return boolean TRUE
     */
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_kurikulum');

        return TRUE;
    }

    /**
     * This function is used to show the kurikulum information
     * @param number $id : This is kurikulum id
     * @return array $result : This is result of query
     */
    function show($id)
    {
        $result = $this->db->get_where('tbl_kurikulum', array('id' => $id))->result();
        return $result;
    }

    /**
     * This function is used to get the kurikulum listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */

    function kurikulumCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_kurikulum as tk');
        if(!empty($searchText)) {
            $likeCriteria = "(tk.id  LIKE '%".$searchText."%'
                            OR  tk.nama  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();

        return count($query->result());
    }

    /**
     * This function is used to get lasted id from the kurikulum information
     * @return  number $id : This is lasted kurikulum id
     */
    function lasted_id(){
        $this->db->select('id');
        $this->db->from('tbl_kurikulum');
        $query = $this->db->get()->last_row();
        $id = $query->id + 1;
        return $id;
    }

    /**
     * This function is used to check nama is already exist or not
     * @param {string} $nama : This is nama
     * @return {mixed} $result : This is searched result
     */
    function check_nama_exists($nama)
    {
        $this->db->select("nama");
        $this->db->from("tbl_kurikulum");
        $this->db->where("nama", $nama);
        $query = $this->db->get();

        return $query->result();
    }
}

?>
