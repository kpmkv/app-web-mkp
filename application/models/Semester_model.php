<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Semester_model extends CI_Model
{

    /**
     * This function return all of the record tbl_semester
     * @param array $dataSemester : This is value of record
     * @return boolean TRUE : This is last inserted id
     */

    function create($dataSemester)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_semester', $dataSemester);

        $this->db->trans_complete();

        return TRUE;
    }



    /**
     * This function is used to get the semester listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function read($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('tbl_semester as ts');
        if(!empty($searchText)) {
            $likeCriteria = "(ts.id  LIKE '%".$searchText."%'
                            OR  ts.nama  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     * This function is used to update the semester information
     * @param array $dataKategori : This is semester updated information
     * @param number $id : This is semester id
     * @return boolean TRUE
     */
    function update($dataSemester, $id)
    {
        $this->db->where('id', $id);

        return $this->db->update('tbl_semester', $dataSemester);
    }

    /**
     * This function is used to delete the semester information
     * @param number $id : This is semester id
     * @return boolean TRUE
     */
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_semester');

        return TRUE;
    }

    /**
     * This function is used to show the semester information
     * @param number $id : This is semester id
     * @return array $result : This is result of query
     */
    function show($id)
    {
        $result = $this->db->get_where('tbl_semester', array('id' => $id))->result();
        return $result;
    }

    /**
     * This function is used to get the semester listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */

    function semesterCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_semester as ts');
        if(!empty($searchText)) {
            $likeCriteria = "(ts.id  LIKE '%".$searchText."%'
                            OR  ts.nama  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();

        return count($query->result());
    }

    /**
     * This function is used to get lasted id from the semester information
     * @return  number $id : This is lasted semester id
     */
    function lasted_id(){
        $this->db->select('id');
        $this->db->from('tbl_semester');
        $query = $this->db->get()->last_row();
        $id = $query->id + 1;
        return $id;
    }

    /**
     * This function is used to check nama is already exist or not
     * @param {string} $nama : This is nama
     * @return {mixed} $result : This is searched result
     */
    function check_nama_exists_semester($nama)
    {
        $this->db->select("nama");
        $this->db->from("tbl_semester");
        $this->db->where("nama", $nama);
        $query = $this->db->get();

        return $query->result();
    }

    function get()
    {
        $this->db->select('*');
        $this->db->from('tbl_semester');
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }
}

?>
