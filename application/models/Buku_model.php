<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Buku_model extends CI_Model
{

    /**
     * This function return all of the record tbl_master_buku
     * @param array $dataBuku : This is value of record
     * @return boolean TRUE : This is last inserted id
     */

    function create($dataBuku)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_master_buku', $dataBuku);

        $this->db->trans_complete();

        return TRUE;
    }



    /**
     * This function is used to get the Buku listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function read($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('tbl_master_buku as tb');
        if(!empty($searchText)) {
            $likeCriteria = "(tb.id  LIKE '%".$searchText."%'
                            OR  tb.nama  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->limit($page, $segment);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    /**
     * This function is used to update the Buku information
     * @param array $dataBuku : This is Buku updated information
     * @param number $id : This is Buku id
     * @return boolean TRUE
     */
    function update($dataBuku, $id)
    {
        $this->db->where('id', $id);

        return $this->db->update('tbl_master_buku', $dataBuku);
    }

    /**
     * This function is used to delete the Buku information
     * @param number $id : This is Buku id
     * @return boolean TRUE
     */
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_master_buku');

        return TRUE;
    }

    /**
     * This function is used to show the Buku information
     * @param number $id : This is Buku id
     * @return array $result : This is result of query
     */
    function show($id)
    {
        $result = $this->db->get_where('tbl_master_buku', array('id' => $id))->result();
        return $result;
    }

    /**
     * This function is used to get the Buku listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */

    function BukuCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('tbl_master_buku as tb');
        if(!empty($searchText)) {
            $likeCriteria = "(tb.id  LIKE '%".$searchText."%'
                            OR  tb.nama  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();

        return count($query->result());
    }

    /**
     * This function is used to get lasted id from the Buku information
     * @return  number $id : This is lasted Buku id
     */
    function lasted_id(){
        $this->db->select('id');
        $this->db->from('tbl_master_buku');
        $query = $this->db->get()->last_row();
        $id = $query->id + 1;
        return $id;
    }

    /**
     * This function is used to check nama is already exist or not
     * @param {string} $nama : This is nama
     * @return {mixed} $result : This is searched result
     */
    function check_nama_exists_buku($nama)
    {
        $this->db->select("nama");
        $this->db->from("tbl_master_buku");
        $this->db->where("nama", $nama);
        $query = $this->db->get();

        return $query->result();
    }
}

?>
