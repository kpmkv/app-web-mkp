<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login";
$route['404_override'] = 'error';


/*********** USER DEFINED ROUTES *******************/

$route['loginMe'] = 'login/loginMe';
$route['dashboard'] = 'user';
$route['logout'] = 'user/logout';
$route['userListing'] = 'user/userListing';
$route['userListing/(:num)'] = "user/userListing/$1";
$route['addNew'] = "user/addNew";

$route['addNewUser'] = "user/addNewUser";
$route['editOld'] = "user/editOld";
$route['editOld/(:num)'] = "user/editOld/$1";
$route['editUser'] = "user/editUser";
$route['deleteUser'] = "user/deleteUser";
$route['loadChangePass'] = "user/loadChangePass";
$route['changePassword'] = "user/changePassword";
$route['pageNotFound'] = "user/pageNotFound";
$route['checkEmailExists'] = "user/checkEmailExists";

$route['forgotPassword'] = "login/forgotPassword";
$route['resetPasswordUser'] = "login/resetPasswordUser";
$route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
$route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['createPasswordUser'] = "login/createPasswordUser";

/*start route of kelas*/
$route['daftarKelas'] = 'kelas/read';
$route['daftarKelas/(:num)'] = "kelas/read/$1";
$route['formTambahKelas'] = "kelas/form_create";
$route['tambahKelas'] = "kelas/create";
$route['formEditKelas'] = "kelas/form_update";
$route['formEditKelas/(:num)'] = "kelas/form_update/$1";
$route['editKelas'] = "kelas/update";
$route['deleteKelas'] = "kelas/delete";
$route['checkNamaExists'] = "kelas/check_nama_exists";
/*end route of kelas*/

/*start route of kategori*/
$route['daftarKategori'] = 'kategori/read';
$route['daftarKategori/(:num)'] = "kategori/read/$1";
$route['formTambahKategori'] = "kategori/form_create";
$route['tambahKategori'] = "kategori/create";
$route['formEditKategori'] = "kategori/form_update";
$route['formEditKategori/(:num)'] = "kategori/form_update/$1";
$route['editKategori'] = "kategori/update";
$route['deleteKategori'] = "kategori/delete";
$route['checkNamaExists'] = "kategori/check_nama_exists";
/*end route of kategori*/

/*start route of Jenjang*/
$route['daftarJenjang'] = 'jenjang/read';
$route['daftarJenjang/(:num)'] = "jenjang/read/$1";
$route['formTambahJenjang'] = "jenjang/form_create";
$route['tambahJenjang'] = "jenjang/create";
$route['formEditJenjang'] = "jenjang/form_update";
$route['formEditJenjang/(:num)'] = "jenjang/form_update/$1";
$route['editJenjang'] = "jenjang/update";
$route['deleteJenjang'] = "jenjang/delete";
$route['checkNamaExists'] = "jenjang/check_nama_exists";
/*end route of Jenjang*/

/*start route of kurikulum*/
$route['daftarKurikulum'] = 'kurikulum/read';
$route['daftarKurikulum/(:num)'] = "kurikulum/read/$1";
$route['formTambahKurikulum'] = "kurikulum/form_create";
$route['tambahKurikulum'] = "kurikulum/create";
$route['formEditKurikulum'] = "kurikulum/form_update";
$route['formEditKurikulum/(:num)'] = "kurikulum/form_update/$1";
$route['editKurikulum'] = "kurikulum/update";
$route['deleteKurikulum'] = "kurikulum/delete";
$route['checkNamaExists'] = "kurikulum/check_nama_exists";
/*end route of kurikulum*/

/*start route of Semester*/
$route['daftarSemester'] = 'semester/read';
$route['daftarSemester/(:num)'] = "semester/read/$1";
$route['formTambahSemester'] = "semester/form_create";
$route['tambahSemester'] = "semester/create";
$route['formEditSemester'] = "semester/form_update";
$route['formEditSemester/(:num)'] = "semester/form_update/$1";
$route['editSemester'] = "semester/update";
$route['deleteSemester'] = "semester/delete";
$route['checkNamaExists'] = "semester/check_nama_exists";
/*end route of Semester*/

/*start route of Buku*/
$route['daftarBuku'] = 'buku/read';
$route['daftarBuku/(:num)'] = "buku/read/$1";
$route['formTambahBuku'] = "buku/form_create";
$route['tambahBuku'] = "buku/create";
$route['formEditBuku'] = "buku/form_update";
$route['formEditBuku/(:num)'] = "buku/form_update/$1";
$route['editBuku'] = "buku/update";
$route['deleteBuku'] = "buku/delete";
$route['checkNamaExists'] = "buku/check_nama_exists";
/*end route of Buku*/



/* End of file routes.php */
/* Location: ./application/config/routes.php */